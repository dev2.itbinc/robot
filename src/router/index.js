import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import PathRobot from '@/components/PathRobot'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'PathRobot',
      component: PathRobot
    }
  ]
})
